/** OK
 * Copyright 2019-present, Facebook, Inc. All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * Messenger For Original Coast Clothing
 * https://developers.facebook.com/docs/messenger-platform/getting-started/sample-apps/original-coast-clothing
 */

"use strict";

// Imports dependencies
const Response = require("./response"),
  i18n = require("../i18n.config"),
  config = require("./config");

module.exports = class Order {
  static handlePayload(payload, ocorrencia, fator, tipo, aeronave, recomendacao) {
    this.ocorrencia = ocorrencia
    this.fator = fator
    this.tipo = tipo
    this.aeronave = aeronave
    this.recomendacao = recomendacao
    let response;

    switch (payload) {
      case "TRACK_ORDER":
        response = Response.genQuickReply(i18n.__("order.prompt"), [
          {
            title: i18n.__("order.account"),
            payload: "LINK_ORDER"
          },
          {
            title: i18n.__("order.search"),
            payload: "SEARCH_ORDER"
          },
          {
            title: i18n.__("menu.help"),
            payload: "CARE_ORDER"
          }
        ]);
        break;

      case "SEARCH_ORDER":
        response = Response.genText(i18n.__("order.number"));
        break;
      
      case "FOUND_ERROR":
        response = Response.genQuickReply(
          i18n.__("fallback.codigoErrado"), [
            {
              title: i18n.__("care.again"),
              payload: "GET_STARTED"
            },
            {
              title: i18n.__("care.leave"),
              payload: "SAIR"
            }
          ]
        )
        break;

      case "ORDER_NUMBER":
        console.log(this.fator)
        response = Response.genQuickReply(
          i18n.__("order.prompt", {
            classificacao: this.ocorrencia.classificacaoOcorrencia,
            cidade: this.ocorrencia.cidadeOcorrencia,
            uf: this.ocorrencia.ufOcorrencia,
            pais: this.ocorrencia.paisOcorrencia,
            data: this.ocorrencia.dataOcorrencia,
            hora: this.ocorrencia.horaOcorrencia,
            relatorio: this.ocorrencia.relatorioDivulgado,
            numeroRecomendacoes: this.ocorrencia.recomendacoesRelatorio,
            numeroAeronaves: this.ocorrencia.quantidadeAeronaves,
            nomeFator: this.fator.nomeFator,
            aspectoFator: this.fator.aspectoFator,
            fatorCondicionante: this.fator.fatorCondicionante,
            areaFator: this.fator.fatorArea,
            tipoOcorrencia: this.tipo.tipoOcorrencia,
            categoriaOcorrencia: this.tipo.categoriaOcorrencia,
            taxonomiaOcorrencia: this.tipo.taxonomiaOcorrencia,
            aeronaveMatricula: this.aeronave.aeronaveMatricula,
            aeronaveTipo: this.aeronave.aeronaveTipo,
            aeronaveFabricante: this.aeronave.aeronaveFabricante,
            aeronaveModelo: this.aeronave.aeronaveModelo,
            aeronaveAno: this.aeronave.aeronaveAno,
            aeronavePais: this.aeronave.aeronavePaisFabricante,
            aeronaveSegmento: this.aeronave.aeronaveSegmento,
            aeronaveFase: this.aeronave.aeronaveFaseOperacao,
            recomendacaoNumero: this.recomendacao.recomendacaoNumero,
            recomendacaoConteudo: this.recomendacao.recomendacaoConteudo,
            recomendacaoDestinatario: this.recomendacao.recomendacaoDestinatario
          }),
          [
            {
              title: i18n.__("care.order"),
              payload: "GET_STARTED"
            },
            {
              title: i18n.__("care.leave"),
              payload: "SAIR"
            }
          ]
        )
        break;

      case "LINK_ORDER":
        response = [
          Response.genText(i18n.__("order.dialog")),
          Response.genText(i18n.__("order.searching")),
          Response.genImageTemplate(
            `${config.appUrl}/order.png`,
            i18n.__("order.status")
          )
        ];
        break;
    }

    return response;
  }
};
